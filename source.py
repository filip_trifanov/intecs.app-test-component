import csv
from pathlib import Path

SOURCE_FILE_PATH = Path('./input.csv')
RESULT_FILE_PATH = Path('./output.csv')

PARAM_PRINT_LINES = True

print('Running...')
with open(SOURCE_FILE_PATH, 'r') as input, open(RESULT_FILE_PATH, 'w+', newline='') as out:
    reader = csv.DictReader(input)
    new_columns = reader.fieldnames
    # append row number col
    new_columns.append('row_number')
    writer = csv.DictWriter(out, fieldnames=new_columns, lineterminator='\n', delimiter=',')
    writer.writeheader()
    for index, l in enumerate(reader):
        # print line
        if PARAM_PRINT_LINES:
            print(f'Printing line {index}: {l}')
        # add row number
        l['row_number'] = index
        writer.writerow(l)